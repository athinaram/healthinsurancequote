<!-- @AUTHOR SIVA KUMAR ATHINARAM @DATE: 08/29/2017 -->
(function() {
	angular
			.module('HealthInsurance', [ 'ui.router' ])
			.constant('APP_NAME', 'Health Insurance Quote')
			.config(function($stateProvider, $urlRouterProvider) {
				$stateProvider.state('home', {
					url : '/',
					templateUrl : 'home.html',
					controller : 'HealthQuoteController'
				}).state('404', {
					url : '/404',
					templateUrl : 'error404.html'
				});
				// if the route not found it will route to below route
				$urlRouterProvider.otherwise("404");
			})
			.controller(
					'HealthQuoteController',
					function($scope, $log, PremiumCalculatorService) {
						// Initializing the form with empty values on the every
						// page load
						$scope.healthform = {
							'name' : '',
							'gender' : '',
							'age' : '',
							'hypertension' : '',
							'bloodpressure' : '',
							'bloodsugar' : '',
							'overweight' : '',
							'smoking' : '',
							'alcohol' : '',
							'dailyexcercise' : '',
							'drugs' : ''
						}
						//Form submit call this function to calculate the premium
						$scope.calculatepremium = function(form) {
							var premiumAmount = 0;
							premiumAmount = PremiumCalculatorService.calculatePremiumForAge(form);
							
							if (form.gender === 'M') {
								premiumAmount = PremiumCalculatorService.calculatePremiumForMale(premiumAmount);
							}
							premiumAmount = PremiumCalculatorService.calculatePremiumForCurrentHealthConditions(form,premiumAmount);
							
							premiumAmount = PremiumCalculatorService.calculatePremiumForHabits(form, premiumAmount);
							
							// Logging the final calculated premium amount
							$log.info($scope.premium = 'Health Insurance Premium for Mr.'+ form.name +': Rs. '+ Math.round(premiumAmount));
							// Passing the final calculated premium anount to
							// html file to display
							$scope.premium = 'Health Insurance Premium for Mr.'+ form.name +': Rs. '+ Math.round(premiumAmount);
						}

					}).service('PremiumCalculatorService',function() {

						var basePremium = 5000;

						var basePercentage = (10 / 100);

						var basePremium18And25 = basePremium + (basePremium * basePercentage);

						var basePremium25And30 = basePremium18And25 + (basePremium18And25 * basePercentage);

						var basePremium30And35 = basePremium25And30 + (basePremium25And30 * basePercentage);

						var basePremium35And40 = basePremium30And35 + (basePremium30And35 * basePercentage);

						var base40PlusPremium = basePremium35And40;
						
						//Calculating the base premium based on age
						
						this.calculatePremiumForAge = function(form) {

							var formdata = form;

							var age = formdata.age;
							if (age < 18) {

								return basePremium;

							} else if (age >= 18 && age < 25) {

								return basePremium18And25;

							}

							else if (age >= 25 && age < 30) {

								return basePremium25And30;

							}

							else if (age >= 30 && age < 35) {

								return basePremium30And35;

							}

							else if (age >= 35 && age < 40) {

								return basePremium35And40;

							}

							else if (age >= 40) {

								var currentAge = age - 40;
								var ageQuotient = Math.floor(currentAge / 5);
								var ageReminder = currentAge % 5;

								if (ageReminder > 0) {
									ageQuotient = ageQuotient + 1;

								}

								for (var i = 1; i <= ageQuotient; i++) {

									base40PlusPremium = base40PlusPremium
											+ (20 / 100);

								}

								return base40PlusPremium;

							}

						}
						//Calculating the base premium based on Habits
						this.calculatePremiumForHabits = function(form,
								currentPremium) {

							var count = 0;

							if (form.smoking === 'Yes') {
								count++;
							}
							if (form.alcohol === 'Yes') {
								count++;
							}
							if (form.drugs === 'Yes') {
								count++;
							}

							if (form.dailyexcercise === 'Yes') {
								if (count > 0) {
									count = count - 1;
								} else {
									count = count + 1;
								}

							}

							return currentPremium
									+ (currentPremium * (count * (3 / 100)));

						}

						//Calculating the base premium based on Gender
						this.calculatePremiumForMale = function(currentPremium) {
							return currentPremium + (currentPremium * (2 / 100));
						}

						//Calculating the base premium based on User pre-existing health condition
						this.calculatePremiumForCurrentHealthConditions = function(form,currentPremium) {

							var count = 0;

							if (form.hypertension === 'Yes') {
								count++;
							}
							if (form.bloodpressure === 'Yes') {
								count++;
							}
							if (form.bloodsugar === 'Yes') {
								count++;
							}
							if (form.overweight === 'Yes') {
								count++;
							}

							return currentPremium + (currentPremium * (count / 100));
						}
					})
})();
